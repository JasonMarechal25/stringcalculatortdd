#include "Split.h"

#include <ostream>

std::vector<std::string> Split(std::string string, const std::string& delimitor)
{
    std::vector<std::string> result(2);
    size_t pos;
    while ((pos = string.find(delimitor)) != std::string::npos) {
        std::string token = string.substr(0, pos);
        result.push_back(token);
        string.erase(0, pos + delimitor.length());
    }
    result.push_back(string);
    return result;
}

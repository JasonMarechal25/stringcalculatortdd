#include "gtest/gtest.h"
#include "StringCalculator.h"

TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(Calculator, emptyString) {
	StringCalculator stringCalculator;
	EXPECT_EQ(stringCalculator.Add(""), 0);
}

TEST(Calculator, oneNumber)
{
	StringCalculator string_calculator;
	EXPECT_EQ(string_calculator.Add("42"), 42);
	EXPECT_EQ(string_calculator.Add("0"), 0);
}

TEST(Calculator, twoNumbers)
{
	StringCalculator string_calculator;
	EXPECT_EQ(string_calculator.Add("0,0"), 0);
	EXPECT_EQ(string_calculator.Add("1,0"), 1);
	EXPECT_EQ(string_calculator.Add("42,1"), 43);
}

TEST(Calculator, moreNumbers)
{
	StringCalculator string_calculator;
	EXPECT_EQ(string_calculator.Add("0,1,2,3,4,5,6,7,8,9,10"), 55);
}
#include "StringCalculator.h"

#include <iostream>
#include <sstream>

int StringCalculator::Add(std::string numbers)
{
	if (!numbers.empty())
	{
		std::istringstream ss{ numbers };
		std::string word;
		int sum = 0;
		while (getline(ss, word, ',')) {
			sum += std::stoi(word);
		}
		return sum;
	}
	return 0;
}
